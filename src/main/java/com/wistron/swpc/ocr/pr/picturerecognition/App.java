package com.wistron.swpc.ocr.pr.picturerecognition;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.wistron.swpc.ocr.pr.picturerecognition.util.ImageProcess;

/**
 * Hello world!
 *
 */
public class App {
	public static final String FOLDER = "D:/Projected/OCR/NoBarCode143817/";
	// public static final String FOLDER = "D:/Projected/OCR/test/";
	public static final File COMMAND_FILE = new File(FOLDER, "script.sh");

	public static void main(String[] args) throws IOException {
		FileWriter fw = new FileWriter(COMMAND_FILE);
		transPicToGamma(new File(FOLDER), fw);
		fw.close();
	}

	private static void transPicToGamma(File file, FileWriter fw) {
		File[] files = file.listFiles();
		try {
			for (File f : files) {
				if (f.isDirectory()) {
					transPicToGamma(f, fw);
				} else {
					BufferedImage RGB_img = ImageIO.read(f);
					// 讀取高度
					int height = RGB_img.getHeight();
					// 讀取寬度
					int width = RGB_img.getWidth();
					// 灰階轉換
					int[] gray_img = new int[width * height];
					// 灰階轉換
					gray_img = ImageProcess.Gray(RGB_img);
					ImageProcess.Gamma(gray_img, width, height, 0.5, f);

					String fileName = f.getName();
					fileName = fileName.substring(0, fileName.lastIndexOf("."));
					File newFile = new File(f.getParent(), String.format("%s_gamma_1.0.bmp", fileName));
					String command = String.format("python3 remove_noise_y.py %s %s", newFile.getAbsolutePath(),
							new File(newFile.getParent(), String.format("%s_rm_noise.bmp", fileName)).getAbsolutePath());
					fw.write(command);
					fw.write("\n");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Finish");

	}
}
