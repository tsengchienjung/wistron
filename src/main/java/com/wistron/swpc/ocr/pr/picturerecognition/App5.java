package com.wistron.swpc.ocr.pr.picturerecognition;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import com.wistron.swpc.ocr.pr.picturerecognition.util.ImageProcess;
import com.wistron.swpc.ocr.pr.picturerecognition.util.Pair;

/**
 * Hello world!
 *
 */
public class App5 {

	public static final String FILE_NG = "D:\\Projected\\OCR\\NG\\normal.png";
	public static final String FILE_OK = "D:\\Projected\\OCR\\OK\\normal2.png";
	public static final String FILE_TEMP = "D:\\Projected\\OCR\\OK\\test.png";
	public static final String FILE_TEMP2 = "D:\\Projected\\OCR\\OK\\test2.bmp";
	public static final String FILE_TEST = "D:\\Projected\\OCR\\shunli\\2.jpg";
	public static final String FILE_TEST2 = "D:\\Projected\\OCR\\OK\\median.bmp";
	public static final String FILE_TEST3 = "D:\\workspace\\PictureRecognition\\negative.bmp";
	public static final String FILE_TEST4 = "D:\\Projected\\OCR\\OK\\normal2_pro.png";
	public static final String FILE_J = "D:\\Projected\\OCR\\OK\\J.png";

	public static final String OUTPUT_FOLDER = "D:\\Projected\\OCR\\output";

	private static final int WHITE = (0xff000000 | 255 << 16 | 255 << 8 | 255);
	private static final int BLACK = (0xff000000 | 0 << 16 | 0 << 8 | 0);

	private static final int DEFAULT_FLAG = -222;
	private static final int CHAIN_SIZE_LIMIT = 100;

	// RGB相加的誤差值，二點相減超過這個值，就當做是不同的顏色
	private static final int DEVIATION = 4;

	public static void main(String[] args) {
		try {
			BufferedImage img = ImageIO.read(new File(FILE_OK));
			// 讀取高度
			int height = img.getHeight();
			// 讀取寬度
			int width = img.getWidth();

			// Image's RGB
			int[] pixels = new int[width * height];
			int[] newPixels = new int[width * height];

			for (int i = 0; i < newPixels.length; i++) {
				newPixels[i] = DEFAULT_FLAG;
			}
			img.getRGB(0, 0, width, height, pixels, 0, width);

			int[] gamma_img1 = new int[width * height];

//			gamma_img1 = ImageProcess.Gamma(pixels, width, height, 0.3);

			int gammaAverage = 0;
			int max = -1;
			for (int i = 0; i < gamma_img1.length; i++) {
				gammaAverage += gamma_img1[i];
				if (gamma_img1[i] > max) {
					max = gamma_img1[i];
				}
			}

			gammaAverage = (gammaAverage / gamma_img1.length);

			max = (int) (max * 0.8);

			for (int i = 0; i < newPixels.length; i++) {
				if (gamma_img1[i] > gammaAverage) {
					newPixels[i] = BLACK;
				} else {
					newPixels[i] = WHITE;
				}
			}

			// 是否已處理過的flag
			boolean[] process = new boolean[width * height];
			Pair[] positions = new Pair[width * height];

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					// https://docs.oracle.com/javase/7/docs/api/java/awt/image/BufferedImage.html#getRGB(int,%20int,%20int,%20int,%20int[],%20int,%20int)
					// pixel = rgbArray[offset + (y-startY)*scansize + (x-startX)];
					chainProcess(width, height, process, newPixels, x, y, positions, newPixels);
				}
			}

			BufferedImage outputImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // image 轉 BfferedImage
			outputImg.setRGB(0, 0, width, height, newPixels, 0, width);
			ImageIO.write(outputImg, "bmp", new File(OUTPUT_FOLDER, "output.bmp"));

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Finish");
	}

	private static int transXY2Position(int x, int y, int width) {
		return y * width + x;
	}

	private static void chainProcess(int width, int height, boolean[] process, int[] pixels, int x, int y, Pair[] positions, int[] newPixels) {
		// offset + (y-startY)*scansize + (x-startX)
		int position = transXY2Position(x, y, width);

		if (process[position]) {
			// 已處理
			return;
		}
		// X, Y position
		positions[position] = new Pair(x, y);

		// 設為已處理
		process[position] = true;

		// flag
		newPixels[position] = WHITE;

		// 要一並塗顏色的position
		List<Integer> fillColor = new ArrayList<Integer>();
		fillColor.add(position);

		// Chain position
		List<Pair> chain = new LinkedList<Pair>();

		// The position RGB
		// Color pixel = new Color(pixels[position]);

		// 加入chain
		chain.add(positions[position]);

		while (chain.size() != 0) {
			// 取得chain最後一筆的x,y
			Pair temp = chain.get(chain.size() - 1);
			// 比較基準點
			// y * width + x
			int comparePosition = transXY2Position(temp.x, temp.y, width);
			// chain process.
			// left
			if (temp.x - 1 >= 0) {
				judgeColor(temp.x - 1, temp.y, width, height, process, pixels, newPixels, fillColor, chain, comparePosition);
			}
			// Right
			if (temp.x + 1 < width) {
				judgeColor(temp.x + 1, temp.y, width, height, process, pixels, newPixels, fillColor, chain, comparePosition);
			}
			// Top
			if (temp.y - 1 >= 0) {
				judgeColor(temp.x, temp.y - 1, width, height, process, pixels, newPixels, fillColor, chain, comparePosition);
			}
			// Bottom
			if (temp.y + 1 < height) {
				judgeColor(temp.x, temp.y + 1, width, height, process, pixels, newPixels, fillColor, chain, comparePosition);
			}
			chain.remove(temp);
		}

		if (fillColor.size() < CHAIN_SIZE_LIMIT) {
			for (int pos : fillColor) {
				newPixels[pos] = WHITE;
			}
		}
	}

	private static void judgeColor(int tempX, int tempY, int width, int height, boolean[] process, int[] pixels, int[] newPixels, List<Integer> fillColor, List<Pair> chain,
			int comparePosition) {
		// System.out.println(String.format("x = %d, y = %d", tempX, tempY));
		int newPosition = transXY2Position(tempX, tempY, width);
		if (!process[newPosition]) {
			if (compareSameColor(pixels, comparePosition, newPosition)) {
				// newPixels[newPosition] = BLACK;
				// 如果不在誤差值內，認定為不同顏色
			} else {
				fillColor.add(newPosition);
				// 加入chain
				chain.add(new Pair(tempX, tempY));
				// 未處理，設為已處理
				process[newPosition] = true;
			}
		}
	}

	private static boolean compareSameColor(int[] pixels, int comparePosition, int newPosition) {
		// int compare = 0;
		// Color newColor = new Color(pixels[newPosition]);
		// Color compareColor = new Color(pixels[comparePosition]);
		// if (Math.abs(newColor.getRed() - compareColor.getRed()) > DEVIATION) {
		// compare++;
		// }
		// if (Math.abs(newColor.getGreen() - compareColor.getGreen()) > DEVIATION) {
		// compare++;
		// }
		// if (Math.abs(newColor.getBlue() - compareColor.getBlue()) > DEVIATION) {
		// compare++;
		// }
		// return compare >= 1;
		// return Math.abs(getRGBCount(pixels[newPosition]) - getRGBCount(pixels[comparePosition])) > DEVIATION;
		// return Math.abs(getRGBCount(pixels[newPosition]) - colorAverage) > DEVIATION;
		return pixels[newPosition] != pixels[comparePosition];
	}

	/**
	 * Because the int rgb value may be -12345,
	 * so need to count it to nature number.
	 * 
	 * @param rgb
	 * @return
	 */
	private static int getRGBCount(int rgb) {
		Color color = new Color(rgb);
		return color.getRed() + color.getGreen() + color.getBlue();
	}

	// private static Color getAverage(BufferedImage img, int width, int y) {
	// int deviation = 0;
	// long rA = 0, gA = 0, bA = 0;
	// for (int x = 0; x < width; x++) {
	// Color color = new Color(img.getRGB(x, y));
	// rA += color.getRed();
	// gA += color.getGreen();
	// bA += color.getBlue();
	// }
	// int r = (int) (rA / width) + deviation;
	// int g = (int) (gA / width) + deviation;
	// int b = (int) (bA / width) + deviation;
	// System.out.println(String.format("y= %d, r = %d, g = %d, b = %d", y, r, g, b));
	// return new Color(r, g, b);
	// }
}
