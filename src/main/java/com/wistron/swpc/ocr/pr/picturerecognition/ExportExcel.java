package com.wistron.swpc.ocr.pr.picturerecognition;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExportExcel {
	public static final String RESULT_FOLDER = "D:/Projected/OCR/NoBarCode143817_result/";
	public static final String FOLDER_NAME = "NoBarCode143817_result";
	public static final String TEMP_FILE = "D:/temp/tmp.jpg";

	private static int index = 1;
	private static FileFilter ff = new FileFilter() {

		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			if (f.getName().endsWith(".bmp") && !(f.getName().contains("noise") || f.getName().contains("gamma"))) {
				return true;
			}
			return false;
		}
	};

	public static void main(String[] args) throws Exception {
		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
		CreationHelper helper = workbook.getCreationHelper();
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Report");

		getFilesResult(new File(RESULT_FOLDER), sheet, helper, workbook);

		// Write the output to a file
		FileOutputStream fileOut = new FileOutputStream(new File(RESULT_FOLDER, "reportfile.xlsx"));
		workbook.write(fileOut);
		fileOut.close();

		// Closing the workbook
		workbook.close();
	}

	private static void getFilesResult(File file, Sheet sheet, CreationHelper helper, Workbook workbook) {
		File[] files = file.listFiles(ff);
		for (File f : files) {
			if (f.isDirectory()) {
				getFilesResult(f, sheet, helper, workbook);
			} else {
				int cellIndex = 0;
				Row row = sheet.createRow(index);
				row.createCell(cellIndex++).setCellValue((index / 7) + 1);
				row.createCell(14).setCellValue(f.getPath().substring(f.getPath().indexOf(FOLDER_NAME) + FOLDER_NAME.length()) + f.getName());

				insertPicture(f, sheet, helper, workbook, row.getRowNum(), ++cellIndex);

				// Gamma
				samePicInsert("gamma", sheet, helper, workbook, f, cellIndex, row);

				cellIndex += 5;

				// noise
				samePicInsert("noise", sheet, helper, workbook, f, cellIndex, row);

				index += 7;
			}
		}
	}

	private static void samePicInsert(String key, Sheet sheet, CreationHelper helper, Workbook workbook, File f, int cellIndex, Row row) {
		File[] noise = findPicture(f, key);
		for (File no : noise) {
			if (no.getName().contains("__")) {
				// 有辨識的結果
				insertPicture(no, sheet, helper, workbook, row.getRowNum(), cellIndex + 4);
				Pattern p = Pattern.compile(".*?__(\\d+).bmp");
				Matcher m = p.matcher(no.getName());
				if (m.find()) {
					row.createCell(cellIndex + 5).setCellValue(m.group(1));
				}
			} else {
				insertPicture(no, sheet, helper, workbook, row.getRowNum(), cellIndex + 2);
			}
		}
	}

	private static File[] findPicture(File f, String key) {
		File[] files = f.getParentFile().listFiles();
		List<File> target = new ArrayList<File>();
		String name = f.getName();
		name = name.substring(0, name.lastIndexOf("."));
		for (File fi : files) {
			if (fi.getName().indexOf(name) != -1 && fi.getName().contains(key)) {
				target.add(fi);
			}
		}
		File[] t = new File[target.size()];
		int index = 0;
		for (File tar : target) {
			t[index] = tar;
			index++;
		}
		return t;
	}

	private static void insertPicture(File f, Sheet sheet, CreationHelper helper, Workbook workbook, int rowIndex, int cellIndex) {
		InputStream is = null;
		try {
			BufferedImage img = ImageIO.read(f);
			ImageIO.write(img, "jpg", new File(TEMP_FILE));

			is = new FileInputStream(TEMP_FILE);
			byte[] bytes = IOUtils.toByteArray(is);

			int pictureIndex = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
			is.close();
			Drawing drawingPatriarch = sheet.createDrawingPatriarch();
			ClientAnchor anchor = helper.createClientAnchor();

			anchor.setCol1(cellIndex);
			anchor.setRow1(rowIndex);
			anchor.setCol2(cellIndex + 1);
			anchor.setRow2(rowIndex);
			Picture pict = drawingPatriarch.createPicture(anchor, pictureIndex);
			pict.resize();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
