package com.wistron.swpc.ocr.pr.picturerecognition;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.imageio.ImageIO;

import com.wistron.swpc.ocr.pr.picturerecognition.util.Pair;

/**
 * Hello world!
 *
 */
public class App2 {

	private static final String FILE_NG = "D:\\Projected\\OCR\\NG\\normal.png";
	private static final String FILE_OK = "D:\\Projected\\OCR\\OK\\normal.png";
	private static final String FILE_TEMP = "D:\\Projected\\OCR\\OK\\test.png";
	private static final String FILE_TEST = "D:\\Projected\\OCR\\shunli\\2.jpg";
	private static final String OUTPUT_FOLDER = "D:\\Projected\\OCR\\output";

	private static final int WHITE = (0xff000000 | 255 << 16 | 255 << 8 | 255);
	private static final int BLACK = (0xff000000 | 0 << 16 | 0 << 8 | 0);

	private static final int DEFAULT_FLAG = -222;

	// RGB相加的誤差值，二點相減超過這個值，就當做是不同的顏色
	private static final int DEVIATION = 2;

	public static void main(String[] args) {
		try {
			BufferedImage img = ImageIO.read(new File(FILE_OK));
			// 讀取高度
			int height = img.getHeight();
			// 讀取寬度
			int width = img.getWidth();
			// Image's RGB
			int[] pixels = new int[width * height];
			int[] newPixels = new int[width * height];
			for (int i = 0; i < newPixels.length; i++) {
				newPixels[i] = DEFAULT_FLAG;
			}
			img.getRGB(0, 0, width, height, pixels, 0, width);

			Set<Integer> colorSet = new TreeSet<Integer>();

			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					colorSet.add(getRGBCount(pixels[transXY2Position(x, y, width)]));
				}
			}
			// Set top 100 average
			long average = 0;
			int count = 0;
			for (int colorCount : colorSet) {
				average += colorCount;
				count++;
				if (count >= 100) {
					break;
				}
			}
			average /= count;
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					int position = transXY2Position(x, y, width);
					if (getRGBCount(pixels[position]) < average) {
						newPixels[position] = BLACK;
					} else {
						newPixels[position] = WHITE;
					}
				}
			}
			BufferedImage outputImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // image 轉 BfferedImage
			outputImg.setRGB(0, 0, width, height, newPixels, 0, width);
			ImageIO.write(outputImg, "bmp", new File(OUTPUT_FOLDER, "output.bmp"));
			// int[] pixels = new int[width * height];
			// img.getRGB(0, 0, width, height, pixels, 0, width);
			// ImageProcess.Sobel(pixels, width, height);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Finish");
	}

	private static int transXY2Position(int x, int y, int width) {
		return y * width + x;
	}

	/**
	 * Because the int rgb value may be -12345,
	 * so need to count it to nature number.
	 * 
	 * @param rgb
	 * @return
	 */
	private static int getRGBCount(int rgb) {
		Color color = new Color(rgb);
		return color.getRed() + color.getGreen() + color.getBlue();
	}

}
